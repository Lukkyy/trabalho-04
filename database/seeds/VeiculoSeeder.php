<?php

use Illuminate\Database\Seeder;
use App\Veiculo;

class VeiculoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Veiculo::create([
        	'codmarca'=>'1',
        	'descricao'=>'F250 6.7L Powerstroke',
        	'modelo'=>'2018',
        	'preco'=>'450000.00'

        ]);

        Veiculo::create([
        	'codmarca'=>'2',
        	'descricao'=>'M3 GTR E46',
        	'modelo'=>'2005',
        	'preco'=>'35000.00'

        ]);

        Veiculo::create([
        	'codmarca'=>'3',
        	'descricao'=>'Veneno',
        	'modelo'=>'2013-2014',
        	'preco'=>'92000.00'

        ]);

        Veiculo::create([
        	'codmarca'=>'4',
        	'descricao'=>'Vision Gran Turismo',
        	'modelo'=>'2018',
        	'preco'=>'80000.00'

        ]);

        Veiculo::create([
        	'codmarca'=>'5',
        	'descricao'=>'LaFerrari',
        	'modelo'=>'2014',
        	'preco'=>'560000.00'

        ]);

        Veiculo::create([
        	'codmarca'=>'6',
        	'descricao'=>'Camaro SS',
        	'modelo'=>'2015',
        	'preco'=>'150000.00'

        ]);                        
    }

}
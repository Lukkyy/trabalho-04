<?php

use Illuminate\Database\Seeder;
use App\Marca;

class MarcaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Marca::create([
        	'descricao'=>'Ford',
        	'telefone'=>'31245436'
        ]);
		Marca::create([
        	'descricao'=>'BMW',
        	'telefone'=>'67657482'
        ]);
        Marca::create([
        	'descricao'=>'Lamborghini',
        	'telefone'=>'12456787'
        ]);
        Marca::create([
        	'descricao'=>'Bugatti',
        	'telefone'=>'128690543'
        ]);
        Marca::create([
        	'descricao'=>'Ferrari',
        	'telefone'=>'077456833'
        ]);
        Marca::create([
        	'descricao'=>'Chevrolet',
        	'telefone'=>'8457323452'
        ]);
    	
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Veiculo extends Migration
{

    public function up()
    {
         Schema::create('veiculo', function(blueprint $table){
            $table->increments('id');
            $table->integer('codmarca')->unsigned();
            $table->string('descricao');
            $table->string('modelo');
            $table->decimal('preco');
           
            $table->foreign('codmarca')->references('id')->on('marca');
            $table->timestamps();
 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('veiculo');
    }
}

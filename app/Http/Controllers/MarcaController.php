<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Marca;

class MarcaController extends Controller
{
    public function index() //METODO GET
    {
        //return Marca::with('veiculos')->paginate();

         try{
            return response()->json( [Marca::with('veiculos')->paginate()], 200 );
        }catch( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
        
    }


    public function store(Request $request) //METODO POST
    {
        try{



            $this->validate(
                $request,
                [
                    'descricao' => 'required|min:2',
                    'telefone' => 'required|numeric'
                ]
            );

        $marca = new Marca();
        $marca->fill($request->all());

        $marca->save();

        //return "Cadastro realizado com sucesso!!";

        if( $marca ){
                return response()->json( [$marca], 201 );
            }else{
                return response()->json( ["mensagem" => "Erro ao cadastrar marca"], 400 );
            }

            return $marca;
        }catch ( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
    }

  
    public function show($id) //METODO GET PASSANDO ID
    {
        $marca = Marca::find($id);
        return $marca;
    }

    public function update(Request $request, $id) //METODO PUT PASSANDO ID
    {
        $marca = Marca::find($id);

        $marca->fill($request->all());

        $marca->save();

        return "Registro Atualizado com sucesso!!!";
    }

  
    public function destroy($id) //METODO DELETE PASSANDO ID
    {
        $marca = Marca::find($id);
        $marca->delete();

        return "Registro deletado com sucesso!!";
    }
}

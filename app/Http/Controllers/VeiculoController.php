<?php

namespace App\Http\Controllers;

use App\Veiculo;
use Illuminate\Http\Request;

class VeiculoController extends Controller
{
    public function index()
    {
        //return Veiculo::with('marcas')->paginate();

         try{
            return response()->json( [Veiculo::with('marcas')->paginate()], 200 );
        }catch( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
    }

    public function store(Request $request)
    {
        try{



            $this->validate(
                $request,
                [
                    'descricao' => 'required|min:2',
                    'codmarca' => 'required|numeric',
                    'preco'=>'required|numeric',
                    'modelo'=>'required|min:2'
                ]
            );
        $veiculo = new Veiculo();

        $veiculo->fill($request->all());
        $veiculo->save();

         //return "Cadastro realizado com sucesso!!";
         if( $veiculo ){
                return response()->json( [$veiculo], 201 );
            }else{
                return response()->json( ["mensagem" => "Erro ao cadastrar veiculo"], 400 );
            }

            return $veiculo;
        }catch ( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
    }

    public function show($id)
    {
        $veiculo = Veiculo::find($id);
        return $veiculo;
    }

    public function update(Request $request, $id)
    {
        $veiculo = Veiculo::find($id);

        $veiculo->fill($request->all());

        $veiculo->save();

        return "Registro Atualizado com sucesso!!!";
    }

    public function destroy($id)
    {
        $veiculo = Veiculo::find($id);
        $veiculo->delete();

        return "Registro Deletado com sucesso!!!";
    }
}

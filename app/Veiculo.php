<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Veiculo extends Model
{
    protected $table = 'veiculo';
    protected $fillable = ['codmarca','descricao','modelo','preco'];

     public function marcas(){
    	return $this->belongsto('App\Marca', 'codmarca', 'id');
    }
}
